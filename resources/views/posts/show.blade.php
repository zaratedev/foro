@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $post->title }}</h1>
        <p>
            {{ $post->content }}
        </p>
        <span>{{ $post->user->name }}</span>
    </div>
@endsection
