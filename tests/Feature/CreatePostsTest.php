<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CreatePostsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_create_a_post()
    {
        $title = "Esta es una pregunta";
        $content = "Este es un contenido";
        $slug = "esta-es-una-pregunta";

        $this->signIn();

        $this->get(route('posts.create'))
            ->assertSee('title')
            ->assertSee('content');

        $this->post(route('posts.store'), [
            "title"     => $title,
            "content"   => $content
        ]);

        $this->assertDatabaseHas('posts', [
           'title'      => $title,
           'content' => $content,
           'slug' => $slug,
           'pending'    => true
        ]);
    }

    /** @test */
    public function creating_a_post_requires_authentication()
    {
        $this->post(route('posts.store'))
            ->assertRedirect(route('login'));
    }

    /** @test */
    public function create_post_form_validation()
    {
        $this->signIn()
            ->from(route('posts.create'))
            ->post(route('posts.store'), [
                'title'     => '',
                'content'   => '',
            ])
            ->assertRedirect(route('posts.create'))
            ->assertSessionHasErrorsIn('title')
            ->assertSessionHasErrorsIn('content');
    }
}
