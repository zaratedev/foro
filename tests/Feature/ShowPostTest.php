<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowPostTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_see_the_post_details()
    {
        $user = create(User::class, ['name' => 'Jonathan']);

        $post = make(Post::class);

        $user->posts()->save($post);

        $this->get($post->url)
            ->assertSee($post->title);
    }

    /** @test */
    public function old_urls_are_redirected()
    {
        $user = create(User::class);
        $this->signIn($user);
        $post = make(Post::class, ['title' => 'Old title']);

        $user->posts()->save($post);
        $url = $post->url;
        $post->update(['title' => 'New title']);

        $response = $this->get($url)->assertRedirect($post->url);
    }
}
