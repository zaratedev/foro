<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Post;

class PostTest extends TestCase
{
    /** @test */
    public function adding_a_title_generates_a_slug()
    {
        $post = new Post([
            'title' => 'Como instalar laravel'
        ]);

        $this->assertEquals('como-instalar-laravel', $post->slug);
    }

    /** @test */
    public function editing_the_title_changes_the_slug()
    {
        $post = new Post([
            'title' => 'Como instalar laravel'
        ]);

        $post->title = 'Como instalar Laravel 5.5 LTS';

        $this->assertEquals('como-instalar-laravel-55-lts', $post->slug);
    }
}
